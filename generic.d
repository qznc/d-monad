import std.traits;
import std.functional : toDelegate;
import std.conv : text;
import std.stdio : writeln;

/* Inspired by
http://stackoverflow.com/questions/6328444/can-traits-in-d-be-used-for-type-classes
http://www.haskell.org/haskellwiki/Functor-Applicative-Monad_Proposal
*/

/*
class Functor f where
    map :: (a -> b) -> (f a -> f b)
*/

template Functor (alias T) {
	static assert (false, "No Functor for "~(T.stringof)~". Implement:
	template Functor (alias T:"~(T.stringof)~") {
		"~(T.stringof)~" delegate("~(T.stringof)~") map(A,B)(B function(A) func) {
		...
		}
	}'");
}

/*
class Functor f => Applicative f where
    return :: a -> f a                     "unit"
    (<*>) :: f (a -> b) -> f a -> f b
    (*>) :: f a -> f b -> f b
    (<*) :: f a -> f b -> f a
*/

template Applicative (alias T) {
	static assert (Functor!T, "Applicative requires Functor to be implemented");
	static assert (false, "No Applicative for Type "~(T.stringof));
}

/*
class Applicative m => Monad m where
    (>>=) :: m a -> (a -> m b) -> m b     "bind"
    f >>= x = join $ map f x

    join :: m (m a) -> m a
    join x = x >>= id
*/

template Monad (alias T) {
	static assert (Applicative!T, "Monad requires Applicative to be implemented");
	static assert (false, "No Monad for Type "~(T.stringof));
}

/*
class Monad m => MonadFail m where
    fail :: String -> m a
*/

abstract class Maybe(T) {
	abstract public bool isNothing();
	abstract public T getValue();

	static class Nothing(T) : Maybe!T {
		public override bool isNothing() { return true; }
		public override T getValue() { throw new Exception("No Value"); }
	}

	static class Just(T) : Maybe!T {
		private T value;
		public this(T x) { this.value = x; }
		public override bool isNothing() { return false; }
		public override T getValue() { return value; }
	}

	public static Maybe!T just(T)(T x) { return new Just!T(x); }
	public static Maybe!T nothing(T)() { return new Nothing!T(); }
}

template Functor(alias T:Maybe) {
	Maybe!B delegate(Maybe!A) map(A,B)(B function(A) func)
	{
		return delegate Maybe!B(Maybe!A p) {
			if (p.isNothing()) return Maybe!B.nothing!B();
			return Maybe!B.just!B(func(p.getValue()));
		};
	}
}

template Applicative(alias T:Maybe) {
	Maybe!A retrn(A)(A a) { return Maybe!A.just(a); }
	//  (<*>) :: f (a -> b) -> f a -> f b    ???
	Maybe!B right(A,B)(Maybe!A a, Maybe!B b) { return b; }
	Maybe!B left (A,B)(Maybe!A a, Maybe!B b) { return a; }
}

template Monad(alias T:Maybe) {
	Maybe!B bind(A,B)(Maybe!A a, Maybe!B function(A) func) {
		if (a.isNothing()) return Maybe!B.nothing!B();
		return func(a.getValue());
	}

	Maybe!A join(A)(Maybe!(Maybe!A) x) {
		if (x.isNothing()) return Maybe!A.nothing!A();
		return x.getValue();
	}
}


unittest {
	auto even = function bool(int x) { return x%2 == 0; };
	auto fisEven = Functor!Maybe.map(even);
	auto i42 = Maybe!int.just(42);
	auto r1 = fisEven(i42);
	assert (r1.getValue() == true);
	assert (r1.isNothing() == false);
	auto r2 = fisEven(Maybe!int.nothing!int());
	assert (r2.isNothing() == true);
	auto lft = Applicative!Maybe.left(r1, r2);
	auto rht = Applicative!Maybe.right(r1, r2);
	assert (lft == r1);
	assert (lft != r2);
	assert (rht == r2);
	assert (rht != r1);
	auto mtwice = function Maybe!int(int x) {
		if (x > (int.max/2)) return Maybe!int.nothing!int();
		return Maybe!int.just(x*2);
	};
	auto r3 = Monad!Maybe.bind(i42,mtwice);
	assert (r3.isNothing() == false);
	assert (r3.getValue() == 84, "wrong result: "~text(r3.getValue()));
	auto ii42 = Maybe!(Maybe!int).just(i42);
	assert (ii42.getValue().getValue() == 42);
	auto r4 = Monad!Maybe.join(ii42);
	assert (r4.isNothing() == false);
	assert (r4.getValue() == 42);
}

template Functor (T:double) {
	T delegate(T p) map(A,B)(B function(A) func)
	{
		return delegate T(T p) {
			if (!isNumeric!A) return double.nan;
			if (p > A.max) return double.nan;
			if (p < A.min) return double.nan;
			auto pa = cast(A) p;
			auto ret = func(pa);
			if (!isNumeric!B) return double.nan;
			if (ret > T.max) return double.nan;
			if (ret < T.min) return double.nan;
			return ret;
		};
	}
}

unittest {
	auto twice = function int(int x) { return x*2; };
	auto fisEven = Functor!(double).map(twice);
	auto r1_ = fisEven(42.0);
	auto r2_ = fisEven(double.max);
	assert (r1_ == 84.0, "wrong result: "~text(r1_));
	assert (r2_ != r2_, "double.max twice should be nan not: "~text(r2_));
}

int main() {
	return 0;
}
